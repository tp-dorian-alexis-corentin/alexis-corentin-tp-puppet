#déclarer un node. Modules: Pare-feu (UFW), Apache, MySQL ou MariaDB (au choix), PHP...
node default { 
  class { 'ufw': }

  ufw::allow { 'ssh':
  port => '22'
  }

  ufw::allow { 'http':
  port => '80'
  }

  ufw::allow { 'https':
  port => '443'
  }
 }

 #commit, pull, puppet agent -t, voir si ça plante ou pas.
